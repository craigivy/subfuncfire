const {PubSub} = require('@google-cloud/pubsub');

// Instantiates a client
const pubsub = new PubSub();

/**
 * HTTP Cloud Function.
 *
 * @param {Object} req Cloud Function request context.
 *                     More info: https://expressjs.com/en/api.html#req
 * @param {Object} res Cloud Function response context.
 *                     More info: https://expressjs.com/en/api.html#res
 */
exports.post2pub = async (req, res) => {

    console.log(`Message to topic ${req.body}`);

    console.log(`request body ${JSON.stringify(req.body)}`);
    const dataBuffer = Buffer.from(JSON.stringify(req.body), `utf8`);
    console.log(`request data is ${dataBuffer}`);

    const orderingKey = req.body.key;
    
    const message = {
        data: dataBuffer,
        orderingKey: orderingKey,
      };

    // References an existing topic
    const topic = pubsub.topic('input', {enableMessageOrdering: true});

    // Publishes a message
    try {
        console.log(`publish message with key: ${orderingKey}, data:${dataBuffer}`);
        await topic.publishMessage(message);
        res.status(200).send('Message published.');
    } catch (err) {
        console.error(err);
        res.status(500).send(err);
        return Promise.reject(err);
    }
};