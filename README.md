# subfuncfire
A simple application which uses various GCP services
* Cloud functions
* Pub/Sub
* Firestore Database

![design](https://gitlab.com/craigivy/subfuncfire/-/raw/master/assests/design.png)


## Setup
* Install terrafrom and gcloud
* Login to Google Cloud Platform(GCP) using gcloud
```
gcloud auth login {user}
```
* Create a new GCP project
* Setup the gitlab project on your environment
```
git clone https://gitlab.com/craigivy/subfuncfire.git
cd subfuncfire/terraform
```

## Installing project using terraform
```
terraform init
terraform apply -auto-approve -var="project=YOUR_PROJECT"
```
## Using the application

Create and get 100
```
. post.sh 100
. get.sh 100
```

Using URLs and curl
```
export APIG=$(terraform output -raw api_hostname)

# post and get
curl -H "Content-Type: application/json" -X POST -d '{"key": "akey", "name":"a_name"}' https://${APIG}/name
curl https://${APIG}/name/akey
```

## Addtional info:
### API Gateway
* OpenApi [overview](https://cloud.google.com/api-gateway/docs/openapi-overview)
* Open Api [editor](https://editor.swagger.io/) 
* [Passing data to and from the backend service](https://cloud.google.com/api-gateway/docs/passing-data)
* [Quickstart: Deploy an API on API Gateway using the gcloud command-line tool](https://cloud.google.com/api-gateway/docs/quickstart)
### Cloud Functions
* [Improving Cloud Function cold start time](https://medium.com/@duhroach/improving-cloud-function-cold-start-time-2eb6f5700f6)