const { PubSub } = require('@google-cloud/pubsub');
const Firestore = require('@google-cloud/firestore');
const PROJECTID = process.env.PROJECT;
const COLLECTION_NAME = 'subfuncfire';

// Instantiates a client
const pubsub = new PubSub();

const firestore = new Firestore({
  projectId: PROJECTID,
  timestampsInSnapshots: true,
});

exports.sub2db = async pubsubMessage => {

  const data = JSON.parse(Buffer.from(pubsubMessage.data, 'base64').toString());
  console.debug(`Writing: ${data} with key: ${data.key} to collection ${COLLECTION_NAME} in project ${PROJECTID}`);
  doc = firestore.collection(COLLECTION_NAME).doc(data.key);
  await doc.set(data, { merge: true });
}