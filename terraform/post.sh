#!/bin/bash

export APIG=$(terraform output -raw api_hostname)

for i in $(eval echo {1..$1});
do
  set -x
  curl -H "Content-Type: application/json" -X POST -d '{"key": "key'$i'", "name":"name_'$i'"}' https://${APIG}/name
  { set +x; } 2>/dev/null
   printf "\n"
done