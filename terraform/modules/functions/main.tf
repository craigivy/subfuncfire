data "archive_file" "func_zip" {
  type        = "zip"
  output_path = "${path.root}/work/${var.func_key}.zip"
  source_dir  = "${path.root}/../${var.func_key}"
  excludes    = ["node_modules", ".gcloudignore", "package-lock.json"]
}

resource "google_storage_bucket_object" "archive" {
# name includes sha which allows cloudfunctions to identify modifications
  name       = "${var.func_key}_${data.archive_file.func_zip.output_base64sha256}.zip"
  bucket     = "${var.region}_${var.project}_source_func_bucket"
  source     = data.archive_file.func_zip.output_path
  depends_on = [data.archive_file.func_zip]
}

resource "google_cloudfunctions_function" "func" {
  name                = var.name
  entry_point         = var.func_key
  # https://cloud.google.com/functions/pricing
  available_memory_mb = 128
  runtime             = "nodejs14"
  trigger_http        = true
  source_archive_bucket = google_storage_bucket_object.archive.bucket
  source_archive_object = google_storage_bucket_object.archive.name
  depends_on = [google_storage_bucket_object.archive]
}
