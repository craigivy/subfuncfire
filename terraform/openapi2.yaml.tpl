swagger: "2.0"
info:
  title: name service
  description: "name service deescritpion"
  version: "1.0.0"
schemes:
  - "https"
paths:
  "/name":
    post:
      description: "Add or update the name for a given key."
      operationId: "postName"
      consumes:
      - "application/json"
      x-google-backend:
          address: https://${REGION}-${PROJECT}.cloudfunctions.net/post
      parameters:
      - in: "body"
        name: "body"
        description: "Name object which contains a key and a name"
        required: true
        schema:
          $ref: "#/definitions/NameObj"            
      responses:
        200:
          description: "Success."
          schema:
            type: string
        404:
          description: "Document not found"
  "/name/{key}":
    get:
      description: "Get the name for a given key."
      operationId: "getName"
      x-google-backend:
        address: https://${REGION}-${PROJECT}.cloudfunctions.net/get
        path_translation: [ APPEND_PATH_TO_ADDRESS ]
      parameters:
        -
          name: key
          in: "path"
          required: true
          type: string
      responses:
        200:
          description: "Success."
          schema:
            type: string
        404:
          description: "Document not found"
definitions:
  NameObj:
    type: "object"
    properties:
      key:
        type: "string"
      name:
        type: "string"            