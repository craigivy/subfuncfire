#!/bin/bash

export APIG=$(terraform output -raw api_hostname)
for i in $(eval echo {1..$1});
do
  set -x
  curl -X GET https://${APIG}/name/key$i
  { set +x; } 2>/dev/null
   printf "\n"
done