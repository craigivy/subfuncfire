output "project" {
    value = var.project
}

output "api_hostname" {
    value = google_api_gateway_gateway.name_gw.default_hostname
}

output "post_url" {
    value = module.func_post2pub.https_url
}

output "get_url" {
    value = module.func_get2db.https_url
}