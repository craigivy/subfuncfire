terraform {

  #   backend "gcs" {
  #   bucket  = "tf-state-prod"
  #   prefix  = "terraform/state"
  # }

  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "3.5.0"
    }
    google-beta = {
      source  = "hashicorp/google-beta"
      version = "~> 3.0"
    }

  }
}

provider "google" {

  //  credentials = file("<NAME>.json")

  project = var.project
  region  = var.region
  zone    = "us-central1-c"
}

provider "google-beta" {

  //  credentials = file("<NAME>.json")

  project = var.project
  region  = var.region
}

resource "google_app_engine_application" "app" {
  provider      = google-beta
  location_id   = var.location_id
  database_type = "CLOUD_FIRESTORE"
}

resource "google_project_service" "firestore" {
  service                    = "firestore.googleapis.com"
  depends_on = [google_app_engine_application.app]
  # disable_dependent_services = true
}

resource "google_project_service" "pubsubEnablement" {
  service                    = "pubsub.googleapis.com"
  disable_dependent_services = true
}

resource "google_pubsub_topic" "pubsub" {
  name = "input"

  # labels = {
  #   foo = "input_topic"
  # }
}

resource "google_project_service" "cloudbuild-service" {
  service                    = "cloudbuild.googleapis.com"
  # disable_dependent_services = true
}

resource "google_project_service" "cloudfunctions-service" {
  service                    = "cloudfunctions.googleapis.com"
  # disable_dependent_services = true
}

resource "google_storage_bucket" "func_bucket" {
  name          = "${var.region}_${var.project}_source_func_bucket"
  force_destroy = true
}

data "archive_file" "sub2db_zip" {
  type        = "zip"
  output_path = "${path.root}/work/sub2db.zip"
  source_dir  = "${path.root}/../sub2db"
  excludes    = ["node_modules", ".gcloudignore", "package-lock.json"]
}

resource "google_storage_bucket_object" "archive" {
# name includes sha which allows cloudfunctions to identify modifications
  name       = "sub2db_${data.archive_file.sub2db_zip.output_base64sha256}.zip"
  bucket     = google_storage_bucket.func_bucket.name
  source     = data.archive_file.sub2db_zip.output_path
  depends_on = [data.archive_file.sub2db_zip]
}

resource "google_cloudfunctions_function" "func" {
  name                = "sub2db"
  entry_point         = "sub2db"
  # https://cloud.google.com/functions/pricing
  available_memory_mb = 2048
  max_instances = 2
  runtime             = "nodejs14"
  event_trigger {
      event_type= "google.pubsub.topic.publish"
      resource= "projects/${var.project}/topics/input"   
  }
  source_archive_bucket = google_storage_bucket_object.archive.bucket
  source_archive_object = google_storage_bucket_object.archive.name
  depends_on = [google_project_service.cloudfunctions-service,google_storage_bucket_object.archive]
}

module "func_get2db" {
  source = "./modules/functions"

  func_key = "get2db"
  name = "get"
  region = var.region
  project = var.project

  depends_on = [google_storage_bucket.func_bucket,google_project_service.cloudbuild-service,google_project_service.cloudfunctions-service]
}

module "func_post2pub" {
  source = "./modules/functions"

  func_key = "post2pub"
  name = "post"
  region = var.region
  project = var.project

  depends_on = [google_storage_bucket.func_bucket,google_project_service.cloudbuild-service,google_project_service.cloudfunctions-service]
}

resource "google_project_service" "apigateway" {
  service                    = "apigateway.googleapis.com"
  # disable_dependent_services = true
}

resource "google_project_service" "servicemanagement-service" {
  service                    = "servicemanagement.googleapis.com"
  depends_on = [google_project_service.apigateway]
  disable_dependent_services = true
}

resource "google_project_service" "servicecontrol-service" {
  service                    = "servicecontrol.googleapis.com"
  depends_on = [google_project_service.servicemanagement-service]
  # disable_dependent_services = true
}

resource "google_api_gateway_api" "name_api" {
  provider = google-beta
  api_id   = "name-api"
  depends_on = [google_project_service.servicecontrol-service]
}

# data "template_file" "template" {
#   template = "${file("${path.module}/../openapi2.yaml.tpl")}"
#   vars = {
#     REGION = var.region
#     PROJECT = var.project
#   }
# }

# resource "local_file" "spec" {
#     content = data.template_file.template.rendered 
#     filename = "${path.module}/openapi2.yaml"
# }

resource "google_api_gateway_api_config" "name_cfg" {
  provider      = google-beta
  api           = google_api_gateway_api.name_api.api_id
  api_config_id = "name-cfg"

  openapi_documents {
    document {
      path     = "spec.yaml"
      contents = base64encode(templatefile("${path.module}/openapi2.yaml.tpl", { REGION=var.region, PROJECT=var.project}))
    }
  }

  gateway_config {
    backend_config {
     google_service_account = "${var.project}@appspot.gserviceaccount.com"
    }
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "google_api_gateway_gateway" "name_gw" {
  provider   = google-beta
  api_config = google_api_gateway_api_config.name_cfg.id
  gateway_id = "api-gw"
}


