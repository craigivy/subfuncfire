const Firestore = require('@google-cloud/firestore');
const PROJECTID = process.env.PROJECT;
const COLLECTION_NAME = 'subfuncfire';

const firestore = new Firestore({
    projectId: PROJECTID,
    timestampsInSnapshots: true,
});


/**
 * HTTP Cloud Function.
 *
 * @param {Object} req Cloud Function request context.
 *                     More info: https://expressjs.com/en/api.html#req
 * @param {Object} res Cloud Function response context.
 *                     More info: https://expressjs.com/en/api.html#res
 */
exports.get2db = async (req, res) => {

    console.debug('Requesting: ', req.originalUrl);
 
    let key = req.path;
    key = key.substring(key.lastIndexOf("/")+1);
 
    console.debug(`Reading key: ${key} from collection: ${COLLECTION_NAME} in project: ${PROJECTID}  `);
 
    const collref = firestore.collection(COLLECTION_NAME).doc(key);
    const doc = await collref.get();
    if (!doc.exists) {
        console.log('No document with key:', key);
        res.status(400).send('Document not found');
    } else {
        console.log(`Returned document with key ${key} data: ${doc.data()}`);
        res.status(200).send(doc.data());
    }
};